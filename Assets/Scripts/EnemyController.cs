using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public float speedX = 300.0f;
    public float speedY = 100.0f;
    public GameTimer fireTimer;
    public GameObject firePrefab;
    
    public float fireFrequency = 20.0f;
    public float jumpFrequency = 50.0f;
    public float jumpForce = 100.0f;

    bool m_FacingLeft = true;
    bool is_Grounded = true;
    bool jump = false;
    bool catched = false;
    Rigidbody2D m_Rigidbody2D;
    Animator animator;
    GameManager gameManager;
    AudioSource audioSource;
    AudioClip laserSound;

    // Start is called before the first frame update
    void Start()
    {
        int dice = Random.Range(0, 10);
        m_FacingLeft = dice >= 4;
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        gameManager = FindObjectOfType<GameManager>();
        audioSource = GetComponent<AudioSource>();
        
        fireTimer.SetActions(StartFireAction, StopFireAction);

        laserSound = Resources.Load("Sounds/laser0") as AudioClip;

        if (!m_FacingLeft)
        {
            Flip();
            m_FacingLeft = false;
        }
    }

    void StartFireAction()
    {
        animator.SetBool("Fire", true);
        audioSource.PlayOneShot(laserSound);
        GameObject fire = Instantiate(firePrefab, m_Rigidbody2D.position, Quaternion.identity);
        if(!m_FacingLeft)
        {
            fire.GetComponent<FireController>().Flip();
        }
        
    }

    void StopFireAction()
    {
        animator.SetBool("Fire", false);
    }


    private void Update()
    {
        if (catched)
        {
            m_Rigidbody2D.velocity = new Vector2(0, speedY);
            if (m_Rigidbody2D.position.y > 240)
            {
                Destroy(gameObject);
            }
            return;
           
        }

        float dice_Fire = Random.Range(0.0f, 10000.0f);
        float p_Dice = 10000.0f - fireFrequency;
        
        if (dice_Fire > p_Dice && !fireTimer.IsRunning)
        {
            fireTimer.StartTimer(0.5f);
        }

        float dice_Jump = Random.Range(0.0f, 10000.0f);
        float p_Jump = 10000.0f - jumpFrequency;

        if (is_Grounded)
        {
            jump = dice_Jump > p_Jump && is_Grounded;
        }
        
        
    }

    void FixedUpdate()
    {
        if (catched) return;
      
        float _speed = m_FacingLeft ? -speedX : speedX;
        Vector2 position = m_Rigidbody2D.position;

        is_Grounded = m_Rigidbody2D.velocity.y < 10 && m_Rigidbody2D.velocity.y > -10;

        m_Rigidbody2D.velocity = new Vector2(_speed, m_Rigidbody2D.velocity.y);

        if (position.y < -240)
        {
            transform.position = new Vector2(m_Rigidbody2D.position.x, 240.0f);
        }

        if (is_Grounded && jump)
        {
            m_Rigidbody2D.velocity = new Vector2(0f, jumpForce);
        } 
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.name == "Tilemap_Wall")
        {
            Flip();
            m_Rigidbody2D.velocity = new Vector2(-m_Rigidbody2D.velocity.x, m_Rigidbody2D.velocity.y);
        }
        else if (collision.gameObject.name.Contains("bubbleFire"))
        {
            catched = true;
            animator.SetBool("Catched", true);
            gameManager.EnemyDied();

        }

    }

    private void Flip()
    {
        m_FacingLeft = !m_FacingLeft;
        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
