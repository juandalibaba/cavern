using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    static GameManager instance;

    public GameObject prefabLive;
    GameObject[] livesSprites;

    public int Lives { get; private set; }
    public int Enemies { get; set; }
    public int Level { get; set; }

    bool gameover = false;

    EnemyManager enemyManager;
    AudioSource audioSource;
    AudioClip levelSound;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Lives = 3;
        Level = 1;
        Enemies = 4;
        livesSprites = new GameObject[Lives];
        enemyManager = FindObjectOfType<EnemyManager>();
        

        DrawLives();
        
    }

    void DrawLives()
    {
        for (int i = 0; i < Lives; i++)
        {
            livesSprites[i] = Instantiate(prefabLive, new Vector2(-374 + 40 * i, -225), Quaternion.identity);

        }
    }

    public void PlayerDied()
    {
        if (Lives > 0)
        {
            Lives--;
            Destroy(livesSprites[Lives]);
        }
        if (Lives == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void EnemyDied()
    {
        Enemies--;
        if (Enemies == 0)
        {
            Level++;
            if(Level == 3)
            {
                SceneManager.LoadScene($"End");
            }
            else
            {
                Enemies = 4;
                enemyManager.Restart();
                SceneManager.LoadScene($"Level{Level}Scene");
            }
            
        }
    }

    void OnLevelWasLoaded(int level)
    {
        audioSource = GetComponent<AudioSource>();
        levelSound = Resources.Load("Sounds/level0") as AudioClip;
        audioSource.PlayOneShot(levelSound);
        DrawLives();
    }


}
