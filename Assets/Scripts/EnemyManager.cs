using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject enemyPrefab;


    public int NumOfEnemies { get; set; }

    private void Awake()
    {
        NumOfEnemies = 4;
    }

    private void Start()
    {
        StartCoroutine(YieldEnemies());
    }

    public void Restart()
    {
        NumOfEnemies = 4;
        StartCoroutine(YieldEnemies());
    }

    IEnumerator YieldEnemies()
    {
        for(int i = 0; i < NumOfEnemies; i++)
        {
            yield return new WaitForSeconds(1);
            float x = (i % 2 == 0) ? 170 : -170;
            Instantiate(enemyPrefab, new Vector2(x, 273), Quaternion.identity);
        }
        
    }

}
