using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleFireController : MonoBehaviour
{

    public float speedX = 200;
    public float speedY = 200;
    public float timeX = 1.0f;
    public float timeY = 1.0f;
    public GameTimer bubbleTimer;

    Rigidbody2D m_Rigidbody;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        bubbleTimer.SetActions(BubbleFireStartAction, BubbleFireStopAction);
        bubbleTimer.StartTimer(0.4f);

    }

    void BubbleFireStartAction()
    {
        m_Rigidbody.velocity = new Vector2(-speedX, 0);
    }

    void BubbleFireStopAction()
    {
        bubbleTimer.SetActions(BubbleFireUpStartAction, BubbleFireUpStopAction);
        bubbleTimer.StartTimer(1.0f);
    }

    void BubbleFireUpStartAction()
    {
        animator.SetBool("Up", true);
        m_Rigidbody.velocity = new Vector2(0, speedY);
    }

    void BubbleFireUpStopAction()
    {
        return;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.name.Contains("robot"))
        {
            Destroy(gameObject, 0.05f);
        }

    }

    public void Flip()
    {
        speedX = -speedX;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Rigidbody.position.y > 270)
        {
            Destroy(gameObject);
        }

    }
}
