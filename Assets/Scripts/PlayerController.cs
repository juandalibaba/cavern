using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed = 300.0f;
    public float jumpForce = 100.0f;
    public GameTimer fireTimer;
    public GameTimer dieTimer;
    public GameObject bubbleFirePrefab;

    float horizontal = 0.0f;
    bool jump = true;
    bool grounded = true;
    bool m_FacingLeft = true;
    bool isFiring = false;
    bool isDied = false;
    Rigidbody2D m_Rigidbody2D;
    Animator animator;
    GameManager gameManager;
    AudioSource audioSource;

    AudioClip blowSound, dieSound, jumpSound;


    private void Start()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        fireTimer.SetActions(FireStartAction, FireStopAction);
        gameManager = FindObjectOfType<GameManager>();
        audioSource = GetComponent<AudioSource>();

        blowSound = Resources.Load("Sounds/blow1") as AudioClip;
        dieSound = Resources.Load("Sounds/die0") as AudioClip;
        jumpSound = Resources.Load("Sounds/jump0") as AudioClip;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDied) return;
        horizontal = Input.GetAxisRaw("Horizontal");
        Fire();
        Jump();
        CheckBorders();
    }

    private void FixedUpdate()
    {
        if (isDied) return;

        Move(horizontal);
        grounded = m_Rigidbody2D.velocity.y < 10 && m_Rigidbody2D.velocity.y > -10;
    }

    void FireStartAction()
    {
        isFiring = true;
        audioSource.PlayOneShot(blowSound);
        animator.SetBool("IsFiring", true);
        GameObject fire = Instantiate(bubbleFirePrefab, m_Rigidbody2D.position, Quaternion.identity);
        if (!m_FacingLeft)
        {
            fire.GetComponent<BubbleFireController>().Flip();
        }
    }

    void FireStopAction()
    {
        isFiring = false;
        animator.SetBool("IsFiring", false);
    }

    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isFiring)
        {
            fireTimer.StartTimer(0.50f);
        }
    }

    void Jump()
    {
        jump = Input.GetKeyDown(KeyCode.UpArrow);

        if (grounded && jump)
        {
            audioSource.PlayOneShot(jumpSound);
            m_Rigidbody2D.velocity = new Vector2(0f, jumpForce);
        }

        animator.SetBool("IsJumping", !grounded);
       

    }

    void Move(float horizontal)
    {
        float velocity_x = (horizontal == 0) ? 0 : (horizontal > 0)? speed : - speed;
        Vector2 position = m_Rigidbody2D.position;

        if (position.y < -240)
        {
            transform.position = new Vector2(m_Rigidbody2D.position.x, 240.0f);
        }


        if (horizontal < 0 && !m_FacingLeft || horizontal > 0 && m_FacingLeft)
        {
            Flip();
        }


        m_Rigidbody2D.velocity = new Vector2(velocity_x, m_Rigidbody2D.velocity.y);
        animator.SetFloat("Speed", Mathf.Abs(m_Rigidbody2D.velocity.x));

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name.Contains("fire"))
        {
            dieTimer.SetActions(StartDieAction, StopDieAction);
            Destroy(collision.gameObject);
            dieTimer.StartTimer(0.6f);
        }
    }

    private void StartDieAction()
    {
        isDied = true;
        audioSource.PlayOneShot(dieSound);
        animator.SetBool("IsDied", true);
        m_Rigidbody2D.velocity = new Vector2(0, 400);
    }

    private void StopDieAction()
    {
        gameManager.PlayerDied();

        isDied = false;
        animator.SetBool("IsDied", false);
        m_Rigidbody2D.position = new Vector2(-164, 270);
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingLeft = !m_FacingLeft;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void CheckBorders()
    {
        if (m_Rigidbody2D.position.x >= 340)
        {
            transform.position = new Vector2(340, m_Rigidbody2D.position.y);

        }
    }
}
