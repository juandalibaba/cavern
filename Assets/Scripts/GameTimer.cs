using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour
{

    public bool IsRunning { get; private set; }

    float time;
    Action startAction;
    Action stopAction;

    public void SetActions(Action _startAction, Action _stopAction)
    {
        startAction = _startAction;
        stopAction = _stopAction;
    }

    void Update()
    {
        if (IsRunning)
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {
                StopTimer();
            }
        }
    }

    public void StartTimer(float targetTime)
    {
        if (IsRunning) return;
        time = targetTime;
        startAction();
        IsRunning = true;
    }

    public void StopTimer()
    {
        IsRunning = false;
        stopAction();
    }

}
